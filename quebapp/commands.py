#!/usr/bin/env python
"""

"""
# Author: Mauro Faccin 2014
# -------------------------
# |   This is QuEBApp     |
# -------------------------
# |    License: GPL3      |
# |   see LICENSE.txt     |
# -------------------------
from __future__ import print_function, division
from quebapp import physics


def print_best_community(dendrogram, filename='q_output.txt'):
    """Print best community to screen and file

    Input:
        :dendrogram: quebapp.Dendrogram; results
        :filename: string, file name
    Output:
        :nothing:

    :returns: nothing

    """
    with open(filename, 'w') as fout:
        b_com = dendrogram[dendrogram.best]
        print('best community:  ', b_com, file=fout)
        print('best modularity: ', b_com.modularity, file=fout)
    print(dendrogram.best_community)


def print_dendrogram(
    dendro,
    filename='q_dendrogram.txt',
    logscale=True,
    linsteps=False
):
    """write dendrogram to file
    to be plotted with gnuplot (or other programs)

    Input:
        :dendro: quebapp.Dendrogram, results
        :filename: string, file name to write

    Output:
        :returns: nothing

    """
    import pylab
    import matplotlib.cm as cm

    nnodes = len(dendro[0])
    nlevel = len(dendro)
    best_partition = dendro.best_community.list()
    n_c = max(best_partition) + 1

    with open(filename, 'w') as fout:
        print('communities    modularity', file=fout)
        for lev in dendro:
            print(lev, lev.modularity, file=fout)

    # find and ordering such that no link cross
    order = []

    def ordering(level, node, to_order):
        links = dendro[level].links
        # links starting from node "node"
        link = [link for link in links if link.start == node][0]
        for end in link.end:
            if level == 1:
                to_order.append(end)
            else:
                to_order = ordering(level-1, end, to_order)
        return to_order

    order = ordering(nlevel-1, 0, order)

    class Position():
        def __init__(self, position=0,
                     closeness=1.0,
                     last=False,
                     level=0,
                     node=0,
                     links=None,
                     com=0):
            self.npos = position
            self.pos = position/(nnodes-1)
            self.clo = closeness
            self.last = last
            self.lev = level
            self.com = com
            self.links = links
            self.node = node

    # add node position
    pos = {}
    for nindx, node in enumerate(order):
        com = best_partition[node]
        pos[(0, node)] = Position(position=nindx, closeness=dendro[0].closeness, node=node, com=com)
    for level, lev in enumerate(dendro):
        clo, links = lev.closeness, lev.links
        if level > 0:
            for link in links:
                if len(link.end) > 1:
                    npos = [pos[(level-1, leaf)].npos for leaf in link.end]
                    ncom = [pos[(level-1, leaf)].com for leaf in link.end]
                    if len(set(ncom)) == 1:
                        ncom = ncom[0]
                    else:
                        ncom = n_c
                    pos[(level, link.start)] = Position(
                        position=(max(npos)+min(npos))/2.0,
                        closeness=clo,
                        last=True,
                        level=level,
                        com=ncom,
                        links=link.end
                    )
                else:  # no leafs for this node
                    n_n = pos[(level-1, link.end[0])]
                    pos[(level, link.start)] = Position(
                        position=n_n.npos,
                        closeness=n_n.clo,
                        com=n_n.com,
                        level=n_n.lev
                    )

    # Draw the dendrogram
    pylab.figure()
    if logscale:
        pylab.xscale('log', nonpositive='clip')
    if linsteps:
        pylab.xlabel('Steps')
    else:
        pylab.gca().invert_xaxis()
        pylab.xlabel('Closeness')
    pylab.ylabel('Index')
    pylab.ylim(-0.05, 1.05)
    pylab.yticks([n/(nnodes-1) for n in range(nnodes)], order)
    for cross in pos:
        level, node = cross
        position = pos[cross]
        if position.last:
            leafs = [pos[(level-1, leaf)] for leaf in position.links]
            color = cm.Set1(position.com/n_c) if position.com < n_c else 'k'
            # plot horizontal lines
            for lev in leafs:
                if linsteps:
                    xes = [lev.lev, position.lev]
                else:
                    xes = [lev.clo, position.clo]
                yes = [lev.pos, lev.pos]
                pylab.plot(xes, yes, lw=2.0, c=color)
            # plot vertical line (joining horizontal ones)
            if linsteps:
                xes = [position.lev, position.lev]
            else:
                xes = [position.clo, position.clo]
            yes = [min([leaf.pos for leaf in leafs]), max([leaf.pos for leaf in leafs])]
            pylab.plot(xes, yes, lw=2.0, c=color)

    pylab.show()

    return order
