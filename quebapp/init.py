#!/usr/bin/env python
# Author: Mauro Faccin 2014
# -------------------------
# |   This is QuEBApp     |
# -------------------------
# |    License: GPL3      |
# |   see LICENSE.txt     |
# -------------------------
"""
A small utility to reproduce the results in:
    Community Detection in Quantum Complex Networks
    Mauro Faccin, Piotr Migdał, Tomi H. Johnson, Ville Bergholm, and Jacob D. Biamonte
    Phys. Rev. X 4, 041012 – Published 21 October 2014
    https://journals.aps.org/prx/abstract/10.1103/PhysRevX.4.041012
"""
import argparse
import string
import numpy as np

from quebapp import commands, physics

TOL = 1e-10
AZ = list(map(str, range(10)))+list(string.ascii_uppercase)


def read_edgelist(filename):
    """ Read Hamiltonian from edge list
    """
    edges = {}
    nodes = set()
    with open(filename, 'r') as fin:
        for line in fin:
            line = line.split()
            if len(line) == 2:
                line.append(1.0)
            edges[(line[0], line[1])] = complex(line[2])
            nodes.add(line[0])
            nodes.add(line[1])
    nodemap = dict(zip(sorted(nodes, key=int), range(len(nodes))))

    # write to screen the dictionary used:
    print('WARNING: The following dictionary has been used')
    print('{:>15s} -> {:<10s}'.format('node-name', 'node-index'))
    for item in nodemap.items():
        print('{:>15s} -> {:<10d}'.format(*item))

    nnodes = len(nodes)
    ham = np.zeros([nnodes, nnodes], dtype=complex)

    for (i, j), weight in edges.items():
        ham[nodemap[i], nodemap[j]] = weight
        if i != j:
            ham[nodemap[j], nodemap[i]] = np.conjugate(weight)
    return ham


def main():
    """MAIN
    """
    # Parse command line arguments
    parser = argparse.ArgumentParser(
        description='QuEBApp: Quantum Community Detection'
    )
    parser.add_argument(
        '--edgelist',
        action='store_true',
        help='Read Hamiltonian as list of edges [i j weight(optional)]'
    )
    parser.add_argument(
        'network',
        help='Network file [default=Hamiltonian matrix]'
    )
    parser.add_argument(
        '-0',
        '--time0',
        action='store_true',
        help='Closeness measure with short-time limit [default: long-time limit]'
    )
    parser.add_argument(
        '-t',
        '--type',
        type=str,
        choices=['fidelity', 'transport', 'purity'],
        default='fidelity',
        help='Use a specific closeness measure [default: fidelity]'
    )
    parser.add_argument(
        '-d',
        '--dendrogram',
        action='store_true',
        help='Outputs the resulting dendrogram and modularity'
    )
    args = parser.parse_args()

    # read the Hamiltonian
    if args.edgelist:
        ham = read_edgelist(args.network)
    else:
        ham = np.loadtxt(args.network)

    system = physics.System(ham)

    # find communities
    dendrogram = physics.find_communities(
        system,
        distance=args.type,
        T='zero' if args.time0 else 'infty'
    )

    if args.dendrogram:
        commands.print_best_community(dendrogram)
        commands.print_dendrogram(dendrogram)
    else:
        commands.print_best_community(dendrogram)
